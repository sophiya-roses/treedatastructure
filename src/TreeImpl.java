
public class TreeImpl {

	
	private TreeNode root;
	
	
	public void add(Integer data) {
		
	if(this.root==null) {
		TreeNode nextt = new TreeNode(data);
		this.root=nextt;
 	
    	}
	else {
		this.root.add(data);
	}
    	
		
	}
	
    public void delete(Integer data) {
    	
    	if(this.root==null)
    		return ;
    	else if(this.root.getLeftChild()==null&&this.root.getRightChild()==null)
    		return ;
    	
    	else {
    		this.root.delete(data,this.root);
    	}
		
	}

	
    public Integer search(Integer data) {
    	if(this.root!=null) {
    		
    		return this.root.search(data);	
    	}
    	
		return null;
		
	}
    
    public Integer smallest() {
    	
    	if(this.root==null)
    		return null;
    	else if (this.root.getLeftChild()==null&&this.root.getRightChild()==null)
    		return this.root.getData();
    	else {
    		
    		TreeNode curr =this.root;
    		while(curr!=null&&curr.getLeftChild()!=null) {
    			curr=curr.getLeftChild();
    		}
    		
    		return curr.getData();
    	}
    	
    }
    
    public void preOrder() {
    	//System.out.println(this.root.getData());	
    	//preOrderTraversal(this.root);
    	inOrderTraversal(this.root);
    }
    
    public void preOrderTraversal(TreeNode root) {

    	if(root!=null) {
    		System.out.println(root.getData());
    		preOrderTraversal( root.getLeftChild());
    		preOrderTraversal( root.getRightChild());
    	}
    	
    	
    	
    }
    
    public void inOrderTraversal(TreeNode root) {

    	if(root!=null) {
    	
    		preOrderTraversal( root.getLeftChild());
    		System.out.println(root.getData());
    		preOrderTraversal( root.getRightChild());
    	}
    	
    	
    	
    }
	
}
