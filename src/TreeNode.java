
public class TreeNode {

	private Integer data;
	private TreeNode leftChild;
	private TreeNode rightChild;
	 public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}

	public TreeNode getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(TreeNode leftChild) {
		this.leftChild = leftChild;
	}

	public TreeNode getRightChild() {
		return rightChild;
	}


	public void setRightChild(TreeNode rightChild) {
		this.rightChild = rightChild;
	}


	public Integer search(Integer data) {
	    	if(this.data==data)
	    		return data;
	    	 if(data < this.data && this.getLeftChild()!=null)
	    	return this.leftChild.search(this.getLeftChild().getData());
	    	 if(this.getRightChild()!=null)
	    		return this.getRightChild().search(this.getRightChild().getData());
	    	 return null;
			
		}


	public TreeNode(Integer data) {
		super();
		this.data = data;
	}

	public void add(Integer data) {
		
		if(data<this.data) {
			
			if(this.getLeftChild()==null) {
				TreeNode nextt = new TreeNode(data);
				this.setLeftChild(nextt);
				
			}
			else {
				this.getLeftChild().add(data);
			}
	    }
		else {
			
			if(this.getRightChild()==null) {
				TreeNode nextt = new TreeNode(data);
				this.setRightChild(nextt);
			}
			else {
				this.getRightChild().add(data);
			}
			
		}
	    	
			
    }

//	public void delete(Integer data) {
//		
//		
//		
//	}

	public void delete(Integer data, TreeNode root) {
		TreeNode curr = root;
		TreeNode parent =root;
		boolean isLeftChild=false;
		while(curr!=null &&curr.getData()!=data) {
			parent=curr;
			
			if(data<curr.getData()) {
				curr=curr.getLeftChild();
				isLeftChild=true;
			}
			else {
				curr=curr.getRightChild();
				isLeftChild=false;
			}
		}
		
		if(curr==null)
			return ;
		if(curr.getLeftChild()==null&&curr.getRightChild()==null)
		{
			if(isLeftChild)
				parent.setLeftChild(null);
			else
				parent.setRightChild(null);
		}
		else if(curr.getRightChild()==null) {
			
			if(curr==root)
			root=curr.getLeftChild();
			
			else {
				if(isLeftChild)
				parent.setLeftChild(curr.getLeftChild());
				else
					parent.setRightChild(curr.getLeftChild());	
			}
			
		}
		
	else if(curr.getLeftChild()==null) {
			
			if(curr==root)
			root=curr.getRightChild();
			
			else {
				if(isLeftChild)
					parent.setLeftChild(curr.getRightChild());
					else
						parent.setRightChild(curr.getRightChild());	
			}
			
		}
		
	}
		
	
}
